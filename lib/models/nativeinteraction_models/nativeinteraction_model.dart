class User {
  String a;
  String authorization;
  String appVersion;
  var userid;


  User({this.a, this.authorization, this.userid, this.appVersion});

  User.fromJson(Map<String, dynamic> json) {
    a = json['a'];
    authorization = json['Authorization'];
    userid = json['userId'];
    appVersion = json['appVersion'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['a'] = this.a;
    data['Authorization'] = this.authorization;
    data['userId'] = this.userid;
    data['appVersion'] = this.appVersion;
    return data;
  }
}