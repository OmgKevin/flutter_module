import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_module/provider/base/base_resp.dart';
import 'package:flutter_module/provider/base/key_config.dart';
import 'package:flutter_module/utils/prefixheader.dart';

class MineCoursePage extends StatefulWidget {
  MineCoursePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MineCoursePageState();
  }
}

class _MineCoursePageState extends State<MineCoursePage> with WidgetsBindingObserver {

  MineCourseListBloc _bloc = new MineCourseListBloc();
  FlutterChannelPlugin eventchannel = new FlutterChannelPlugin();
  List<PurchasedCourse> mineCourseList = [];
  String userid;

  @override
  void initState() {
    super.initState();
    _initAsync();
    eventchannel.init((String msg) {});
    eventchannel.responseStream.listen((String msg) async {
      Map userMap = json.decode(msg);
      var user = new User.fromJson(userMap);
      userid = user.userid;

      SpUtil.putString(Keys.authorization, user.authorization);
      SpUtil.putString(Keys.a, user.a);
      SpUtil.putString(Keys.appVersion, user.appVersion);

      _getList();
    });

    WidgetsBinding.instance.addObserver(this);
  }

  void _initAsync() async {
    await SpUtil.getInstance();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive: // 处于这种状态的应用程序应该假设它们可能在任何时候暂停。
        break;
      case AppLifecycleState.resumed: // 应用程序可见，前台
      // _getList();
        break;
      case AppLifecycleState.paused: // 应用程序不可见，后台
        break;
      case AppLifecycleState.suspending: // 申请将暂时暂停
        break;
    }
  }

  _getList() {
    if (userid != null && userid.isNotEmpty) {
      Map<String, String> paramsmap = {'id': userid};
      _bloc.getList(paramsmap);
    }
  }
    Future<Null> _refresh() async {
      // mineCourseList.clear();
      await _getList();
      return;
    }

    @override
    void dispose() {
      WidgetsBinding.instance.removeObserver(this);
      super.dispose();
    }

    @override
    Widget build(BuildContext context) {
      return StreamBuilder<BaseResp>(
        stream: _bloc.mineCourseList,
        // initialData: initialData ,
        builder: (BuildContext context, AsyncSnapshot<BaseResp> snapshot) {
          if (snapshot.data != null && snapshot.hasData) {
            var response = snapshot.data;
            if (response.result == true && response.data != null) {
              mineCourseList = response.data.purchasedCourses;
              // print(mineCourseList);
              if (mineCourseList.length == 0) {
                return RefreshIndicator(
                  onRefresh: _refresh,
                  child: Scaffold(
                      body: Container(
                        color: Colors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            // Image.network(
                            //     'https://xszx-test-1251987637.cos.ap-beijing.myqcloud.com/flutter/images/noCourse.png',
                            //     width: 198,
                            //     height: 182),
                            ExtendedImage.asset("images/noCourse.png",
                                          enableLoadState: false,
                                          width: 198,
                                          height: 182,
                                          ),
                            Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(top: 30),
                              child: Text('您还没有选课，去首页选课进入学习吧！',
                                  style: TextStyle(
                                      color: Color.fromRGBO(153, 153, 153, 1),
                                      fontSize: 13)),
                            )
                          ],
                        ),
                      )),
                );
              } else {
                return Scaffold(
                    body: RefreshIndicator(
                      onRefresh: _refresh,
                      child: Container(
                          color: Colors.white,
                          child: ListView.separated(
                            itemCount: mineCourseList.length,
                            itemBuilder: (context, index) {
                              return MineCourseListItem( map: mineCourseList[index]);
                              },
                            separatorBuilder: (context, index) {
                              return Container(
                                height: 10,
                                color: Color.fromRGBO(247, 247, 247, 1),
                              );
                            },
                          )),
                    ));
              }
            } else {
              return LoadingPage();
            }
          } else {
            return LoadingPage();
          }
        },
      );
      // return
    }
  }


