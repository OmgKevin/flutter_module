import 'package:flutter/material.dart';
import 'package:flutter_module/utils/prefixheader.dart';

class MineCourseListItem extends StatefulWidget {
  final PurchasedCourse map;

  MineCourseListItem({Key key, @required this.map}) : super(key: key);

  _MineCourseListItemState createState() => _MineCourseListItemState();
}

class _MineCourseListItemState extends State<MineCourseListItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('我的课程列表点击');
        if (widget.map.isCourse == 1) {
          showNativeView(1, widget.map.ocId, widget.map.goId);
        } else if (widget.map.isCourse == 2) {
          showNativeView(2, widget.map.ocId, widget.map.goId);
        }
      },
      child: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // 橙色条
          Container(
            height: 22,
            // width: MediaQuery.of(context).size.width - 500.0,
            margin: const EdgeInsets.only(left: 0.0, top: 10.0, right:10.0, bottom: 8.0),
            padding: const EdgeInsets.only(right: 11, top: 2.90, left: 11),
            child: Text(
              widget.map.isCourse == 1 ? "公开课" : '${widget.map.proName}',
              style: TextStyle(color: Color.fromRGBO(243, 116, 43, 1), fontSize: 12,fontWeight: FontWeight.bold),
              softWrap: true,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            decoration: BoxDecoration(
                color: Color.fromRGBO(243, 110, 34, 0.2),
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(11),
                    topRight: Radius.circular(11))),
          ),

          // 内容区
          Container(
            height: 98,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // 左侧
                Container(
                  constraints: BoxConstraints.tight(Size(145, 81)),
                  margin: EdgeInsets.only(left: 16, right: 10),
                  child:ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                     child: widget.map.cover != null &&  widget.map.cover.isNotEmpty ? ExtendedImage.network(
                                          widget.map.isCourse == 2 ? widget.map.cover : widget.map.cover,
                                          cache: true,
                                          enableLoadState: false,
                                          fit: BoxFit.cover)
                                      : ExtendedImage.asset("images/default_photo.png",
                                          enableLoadState: false,
                                          fit: BoxFit.cover)
                  ),
                ),
                // 右侧
                Container(
                  margin: EdgeInsets.only(left: 15, top: 3, right: 10),
                  width: MediaQuery.of(context).size.width - 200.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text( widget.map.isCourse == 2 ? '${widget.map.goName}' : '${widget.map.ocName}',
                        style: TextStyle(fontSize: 17.0,
                          fontWeight: FontWeight.bold),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Padding(
                        padding: EdgeInsets.all(0),
                        child: Row(
                          children: <Widget>[
                            hasTeacher( widget.map.isCourse == 2, widget.map.avatarUrl),
                            Container(
                              padding: EdgeInsets.only(bottom: 18.0),
                              width: 110,
                              child: widget.map.isCourse != 2 ? Text('${widget.map.tname}',
                                      style: TextStyle(fontSize: 15),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis)
                                  : Text('共' + '${widget.map.lessionCount}' + '节',
                                      style: TextStyle(fontSize: 15),
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      )),
    );
  }
}

Widget hasTeacher(bool hasTeacher, String avatarUrl) {
  if (!hasTeacher) {
    return Container(
      margin: EdgeInsets.only(top: 20.0,right: 20.0),
      constraints: BoxConstraints.tight(Size(28, 28)),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(28),
          image: DecorationImage(
              image: NetworkImage(avatarUrl), fit: BoxFit.cover)),
    );
  } else {
    return Container();
  }
}

void showNativeView(int isCourse, String coId, String goId) {
  FlutterChannelPlugin.myCoursesList(isCourse, coId, goId);
}
