import 'package:flutter/material.dart';

class TabbarGoodCourse extends StatefulWidget {
  TabbarGoodCourse({Key key}) : super(key: key);

  @override
  _TabbarGoodCourseState createState() => _TabbarGoodCourseState();
}

class _TabbarGoodCourseState extends State<TabbarGoodCourse>
    with SingleTickerProviderStateMixin {

  TabController mController;
  List<Tab> tabList;


  @override
  void initState() {
    super.initState();
    initTabData();
    mController = TabController(length: tabList.length, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    mController.dispose();
  }

  initTabData() {
    tabList = [
      Tab(text: '好课推荐'),
      Tab(text: '语文'),
      Tab(text: '数学'),
      Tab(text: '政治'),
      Tab(text: '地理'),
      Tab(text: '生物'),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(top: 21.0, left: 17.0,bottom: 21.0),
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(
                          color: Color.fromRGBO(247, 247, 247, 1),
                          width: 10.0))),
              child: Text(
                '合作课程',
                style: TextStyle(
                    fontSize: 17.0,
                    color: Color.fromRGBO(25, 25, 25, 1),
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              color: new Color(0xfff4f5f6),
              height: 46.0,
              child: TabBar(
                isScrollable: false,
                controller: mController,
                labelColor: Colors.black,
                labelStyle:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                unselectedLabelColor: Color(0xff666666),
                indicatorColor: Colors.deepOrange,
                indicatorWeight: 4,
                indicatorPadding: EdgeInsets.only(left: 75, right: 75),
                tabs: tabList.map((item) {
                  return Tab(
                    text: item.text,
                  );
                }).toList(),
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: mController,
                children: tabList.map((item) {
                  return Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: Text(item.text),
                      ),
                    ],
                  );
                }).toList(),
              ),
            )
          ],
        ));
  }
}
