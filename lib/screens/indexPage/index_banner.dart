import 'package:flutter/material.dart';
import 'package:flutter_module/provider/base/base_resp.dart';
import 'package:flutter_module/utils/prefixheader.dart';

class Indexbanner extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _IndexbannerState();
}

class _IndexbannerState extends State<Indexbanner>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin<Indexbanner> {
  @override
  bool get wantKeepAlive => true;
  GetIndexBannerListBloc _bloc = new GetIndexBannerListBloc();
  List<IndexOpenCourseModel> indexBannerList = [];

  _getIndexBanner() {
    Map<String, String> params = {'type': '1'};
    _bloc.getIndexBannerList(params);
  }

  @override
  void initState() {
    super.initState();
    _getIndexBanner();        
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return StreamBuilder<BaseResp>(
        stream: _bloc.indexBannerStream,
        builder: (context, AsyncSnapshot<BaseResp> snapshot) {
          if (snapshot.data != null && snapshot.hasData) {
            var response = snapshot.data;
            if (response.result == true && response.data.length > 0) {
              indexBannerList = response.data;
              if (indexBannerList.length > 1) {
                return Container(
                  padding: EdgeInsets.symmetric(horizontal: 17.0),
                  child: AspectRatio(
                    aspectRatio: 10.0 / 3.46,
                    child: Swiper(
                      itemBuilder: (BuildContext context, int index) {
                        return ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: ExtendedImage.network(
                              indexBannerList[index].picture,
                              cache: true,
                              enableLoadState: false,
                              fit: BoxFit.fill),
                        );
                      },
                      itemCount: indexBannerList.length,
                      autoplay: true,
                      duration: 500,
                      pagination: SwiperPagination(
                        builder: DotSwiperPaginationBuilder(
                            color: Colors.blue[600],
                            activeColor: Colors.white,
                            space: 5,
                            size: 8.0,
                            activeSize: 8.0),
                      ),
                      onTap: (index) {
                        if (indexBannerList[index].linkType == 1) {
                          //传index.url 完整链接 ，true 为开启埋点统计 ，埋点ID ，bannertype类型为1代表跳转webview
                          toWebview(
                            indexBannerList[index].linkUrl,
                            1,
                          );
                        } else if (indexBannerList[index].linkType == 2) {
                          // String router = indexBannerList[index].router.split('?')[0];
                          String classtype = indexBannerList[index]
                              .router
                              .split('?')[1]
                              .split('=')[0];
                          String params = indexBannerList[index]
                              .router
                              .split('?')[1]
                              .split('=')[1];

                          if (classtype == 'openClassId') {
                            // 传bannertype类型为2代表跳转原生页 ，classtype类型为1代表公开课，传参数openClassId
                            FlutterChannelPlugin.toNativePage(2, 1, params);
                          } else if (classtype == 'goId') {
                            // 传bannertype类型为2代表跳转原生页 ，classtype类型为2代表付费课，传参数goId
                            FlutterChannelPlugin.toNativePage(2, 1, params);
                          }
                        }
                      },
                    ),
                  ),
                );
              } else {
                return GestureDetector(
                  onTap: () {
                    print('banner');
                    if (indexBannerList[0].linkType == 1) {
                      toWebview(
                        indexBannerList[0].linkUrl,
                        1,
                      );
                    } else if (indexBannerList[0].linkType == 2) {
                      String classtype =
                          indexBannerList[0].router.split('?')[1].split('=')[0];
                      String params =
                          indexBannerList[0].router.split('?')[1].split('=')[1];
                      if (classtype == 'openClassId') {
                        // 传类型为1 的公开课详情页 ，传参数openClassId
                        FlutterChannelPlugin.toNativePage(2, 1, params);
                      } else if (classtype == 'goId') {
                        // 传类型为2 的付费课详情页 ，传参数goId
                        FlutterChannelPlugin.toNativePage(2, 2, params);
                      }
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 17.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: AspectRatio(
                        aspectRatio: 10.0 / 3.46,
                         child: ExtendedImage.network(
                            indexBannerList[0].picture,
                            cache: true,
                            enableLoadState: false,
                            fit: BoxFit.fill,
                            height: ScreenUtil().setHeight(122),
                        ),
                      ),
                    ),
                  ),
                );
              }
            } else {
              return Container();
            }
          } else {
            return Container();
          }
        });
  }
}

// 跳转广告位 webview
void toWebview(String link, int bannertype) async {
  await FlutterChannelPlugin.toAdPage(link, bannertype);
}
