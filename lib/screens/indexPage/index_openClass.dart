import 'package:flutter/material.dart';
import 'package:flutter_module/provider/base/base_resp.dart';
import 'package:flutter_module/utils/prefixheader.dart';


class IndexOpenClass extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _IndexOpenClassState();
}

class _IndexOpenClassState extends State<IndexOpenClass>
    with WidgetsBindingObserver,AutomaticKeepAliveClientMixin<IndexOpenClass> {
  @override
  bool get wantKeepAlive => true;
  GetopenClassListBloc _bloc = new GetopenClassListBloc();
  List<IndexOpenClassModel> indexClassList = [];
 
  _getIndexBanner() {
    Map<String, String> params = {'type': '2'};
    _bloc.getopenClassList(params);
  }
 

  _dot(val) {
    final count = val.indexOf('.') > 0 ? '万' : '';
    return val + count;
  }

  @override
  void initState() {
    super.initState();
     _getIndexBanner();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return StreamBuilder<BaseResp>(
        stream: _bloc.getopenClassListStream,
        builder: (context, AsyncSnapshot<BaseResp> snapshot) {
          if (snapshot.data != null && snapshot.hasData) {
            var response = snapshot.data;
            if (response.result == true && response.data.length > 0) {
              indexClassList = response.data;
              return Container(
                height: 136.0,
                margin: EdgeInsets.only(top: 19.0),
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: indexClassList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return listItem(indexClassList[index], index);
                    }),
              );
            } else {
              return Container();
            }
          } else {
            return Container();
          }
        });
  }

  // 横向滑动列表
  Widget listItem(item, index) {
    return Container(
      width: 156.0,
      margin: EdgeInsets.only(
          left: index == 0 ? 17.0 : 4.5,
          right: indexClassList.length - 1 == index ? 17.0 : 4.5),
      child: GestureDetector(
        onTap: () {
           FlutterChannelPlugin.toNativePage(2, 1, item.openClassId.toString());
        },
        child: Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(3.0),
              child: Container(
                width: 149.0,
                height: 81.0,
                 child: ExtendedImage.network(
                            item.bannerUrl != null ? item.bannerUrl : 'https://xszx-test-1251987637.cos.ap-beijing.myqcloud.com/flutter/images/courseDefault.png',
                            cache: true,
                            enableLoadState: false,
                            fit: BoxFit.fill,
                        ),
              ),
            ),
            Row(
              children: <Widget>[
                Text(
                  item.learnerCount == null
                      ? ''
                      : _dot(item.learnerCount) + '次',
                  style: TextStyle(
                      fontSize: 12.0,
                      color: Color.fromRGBO(254, 100, 0, 1),
                      fontWeight: FontWeight.bold,
                      height: 2.4),
                ),
                Padding(padding: EdgeInsets.only(left: 8.0)),
                Text(
                  item.learnerCount == null ? '' : '学习',
                  style: TextStyle(
                      fontSize: 12.0,
                      color: Color.fromRGBO(158, 158, 158, 1),
                      fontWeight: FontWeight.bold,
                      height: 2.4),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

