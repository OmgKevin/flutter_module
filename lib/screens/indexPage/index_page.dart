import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_module/utils/prefixheader.dart';

class Index extends StatefulWidget{
  @override 
  State<StatefulWidget> createState() => _IndexState();
}

class _IndexState extends State<Index> with AutomaticKeepAliveClientMixin<Index> {

  @override
  bool get wantKeepAlive => true;
  ScrollController _controller = new ScrollController();
  AppVersionLimitBloc _blocofappversionlimit = new AppVersionLimitBloc();
  
  @override
  void initState() {
    super.initState();
    //_getblocofappversionlimit();
  }


  _getblocofappversionlimit() {
    Map<String, String> paramsmap = {
      'deviceType': Platform.isIOS ? '2' : '1',
      'version': '1.1.0'
    };
    _blocofappversionlimit.getAppVersionLimit(paramsmap);
  }

  @override 
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context){
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        shrinkWrap: true,
        controller: _controller,
        padding: EdgeInsets.only(top: 10.0),
        children: <Widget>[
          Indexbanner(),
          OpenCourse(),
          IndexOpenClass(),
          GoodCourse(),
          Platform.isIOS ? Container(width: MediaQuery.of(context).size.width,height: 80,color: Colors.white) : Container(width: MediaQuery.of(context).size.width,height: 0,color: Colors.white)
        ],
      )
    );
  }
}