import 'package:flutter/material.dart';
import 'package:flutter_module/provider/base/base_resp.dart';
import 'package:flutter_module/utils/prefixheader.dart';

class OpenCourse extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _OpenCourseState();
}

class _OpenCourseState extends State<OpenCourse>
    with WidgetsBindingObserver, AutomaticKeepAliveClientMixin <OpenCourse> {
  @override
  bool get wantKeepAlive => true;
  GetopenClassListBloc _bloc = new GetopenClassListBloc();
  ScrollController _controller = new ScrollController();
  List<IndexOpenClassModel> indexOpenList = [];
 
  _getIndexBanner() {
    Map<String, String> params = {'type': '1'};
    _bloc.getopenClassList(params);
  }

  @override
  void initState() {
    super.initState();
     _getIndexBanner();
  }
 

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return StreamBuilder<BaseResp>(
      stream: _bloc.getopenClassListStream,
      builder: (context, AsyncSnapshot<BaseResp> snapshot) {
        if (snapshot.data != null && snapshot.hasData) {
          var response = snapshot.data;
          if (response.result == true && response.data.length > 0) {
            indexOpenList = response.data;
            return Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: 13.0, left: 17.0, right: 17.0),
                  child: Text(
                    '试听课',
                    style: TextStyle(
                        color: Color.fromRGBO(25, 25, 25, 1),
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    FlutterChannelPlugin.toNativePage(2, 1, indexOpenList[0].openClassId.toString()); 
                  },
                  child: Padding(
                    padding:
                        EdgeInsets.only(top: 14.0, left: 17.0, right: 17.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child: AspectRatio(
                        aspectRatio: 10.0 / 4.63,
                         child: ExtendedImage.network(
                            indexOpenList[0].bannerUrl,
                            cache: true,
                            enableLoadState: false,
                            fit: BoxFit.fill,
                        ),
                      )
                    ),
                  ),
                ),
              ],
            );
          } else {

            return Container(color: Colors.white,margin: EdgeInsets.only(top: 13.0),);
//            return Container(
//              alignment: Alignment.centerLeft,
//              margin: EdgeInsets.only(top: 13.0, left: 17.0, right: 17.0),
//              child: Text(
//                '试听课',
//                style: TextStyle(
//                    color: Colors.black,
//                    fontSize: 17.0,
//                    fontWeight: FontWeight.bold),
//              ),
//            );
          }
        } else {
          return Container();
        }
      },
    );
  }
}

