
import 'package:flutter_module/provider/base/base_api_provider.dart';
import 'package:flutter_module/provider/base/base_resp.dart';
import 'package:flutter_module/provider/base/network_config.dart';

class AppVersionLimitApiProvider extends BaseApiProvider {
  /*
  *  获取app版本限制
  * @links
  */
  Future<BaseResp> getAppVersionLimit(Map<String, dynamic> params) async {
    final response = await get(NetworkConfig.appVersionLimit['appVersionLimit'], params);
    return super.verifyMiddleWare(response);
  }
}