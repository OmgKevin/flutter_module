import 'dart:async';
import 'dart:io';
import 'base_resp.dart';
import 'package:dio/dio.dart';
import 'package:flutter_module/utils/prefixheader.dart';
import 'package:flutter_module/provider/base/base_resp.dart';

import 'key_config.dart';

class BaseApiProvider {
  Dio dio = new Dio();

  BaseApiProvider() {
    dio.options.connectTimeout = 5000;
    dio.options.receiveTimeout = 3000;
    
    // dio请求 设置代理
  //   (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
  //     (HttpClient client) {
  //   client.findProxy = (uri) {
  //     return "PROXY 172.18.70.94:8889";
  //   };
  // };
    
    dio.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
      String timestamp = DateUtils.getNowDateMs().toString(); // 获取当前时间戳
      if (options.method == "POST") {
        options.data['timestamp'] = timestamp;
        String sign = httpUtil.sortMap(options.data);
        options.data['sign'] = sign;
      } else {
        options.queryParameters['timestamp'] = timestamp;
        // 创建sign
        String sign = httpUtil.sortMap(options.queryParameters);
        options.queryParameters['sign'] = sign;
      }

      // 缓存获取token
       await SpUtil.getInstance();
       String authorization = SpUtil.getString(Keys.authorization);
       String a = SpUtil.getString(Keys.a);
       String appVersion = SpUtil.getString(Keys.appVersion);

      if (null != authorization && authorization.isNotEmpty && null != a && a.isNotEmpty && null != appVersion && appVersion.isNotEmpty) {
        options.headers['Authorization'] = authorization;
        options.headers['a'] = a;
        options.headers['appVersion'] = appVersion;
      }

      return options;

    }, 
    onResponse: (Response response) {
      return response;
    }, 
    onError: (DioError e) {

      // token 过期或者失效需要告诉原生当前需要重新登陆
      if( 401 == e.response.statusCode){
        FlutterChannelPlugin.myCoursesList(401, 'singlelogin', 'singlelogin');
      }
      return _onError(e);
    }));
  }

  post(url, [var params]) {
    Completer completer = Completer();
    dio.post(url, data: params).then((res) {
      completer.complete(res);
    }).catchError((error) {
      completer.complete(error);
    });
    return completer.future;
  }

  static const Map<String, dynamic> initParams = {};

  get(url, [var params = initParams]) {
    Completer completer = Completer();
    dio.get(url, queryParameters: params).then((res) {
      completer.complete(res);
    }).catchError((error) {
      completer.complete(_handleError(error));
    });
    return completer.future;
  }

  verifyMiddleWare(dynamic temp) {
    if (temp is Response) {
      final res = temp.data;
      if (200 == res['code'] || '200' == res['code']) {
        if (res['code'] is String) {
          return BaseResp(
              code: int.parse(res['code']),
              msg: res['msg'],
              result: true,
              data: res['data']);
        } else {
          return BaseResp(
              code: res['code'],
              msg: res['msg'],
              result: true,
              data: res['data']);
        }
      } else {
        return BaseResp(code: res['code'], msg: res['msg'], result: false);
      }
    } else {
      return temp;
    }
  }

  _handleError(DioError error) {
    String errorDescription = "";
    if (error is DioError) {
      switch (error.type) {
        case DioErrorType.CANCEL:
          errorDescription = "Request to API server was cancelled";
          break;
        case DioErrorType.CONNECT_TIMEOUT:
          errorDescription = "Connection timeout with API server";
          break;
        case DioErrorType.DEFAULT:
          errorDescription =
              "Connection to API server failed due to internet connection";
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          errorDescription = "Receive timeout in connection with API server";
          break;
        case DioErrorType.RESPONSE:
          errorDescription =
              "Received invalid status code: ${error.response.statusCode}";
          break;
        case DioErrorType.SEND_TIMEOUT:
          errorDescription = "SEND_TIMEOUT";
          break;
      }
    } else {
      errorDescription = "Unexpected error occured";
    }

    return Response(data: {
      "code": CustomRespCode.networkFail,
      "result": false,
      "data": errorDescription
    });
  }

  Future _onError(DioError error) {
    Completer completer = Completer();
    if (error is DioError) {
      switch (error.type) {
        case DioErrorType.CANCEL:
          break;
        case DioErrorType.CONNECT_TIMEOUT:
          break;
        case DioErrorType.DEFAULT:
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          break;
        case DioErrorType.RESPONSE:
          break;
        case DioErrorType.SEND_TIMEOUT:
          break;
      }
    }
    return completer.future;
  }

}
