
import 'package:flutter/material.dart';
import 'package:flutter_module/screens/indexPage/index_page.dart';
import 'package:flutter_module/screens/index_tabbargoodcourse.dart';
import 'package:flutter_module/screens/mineCoursePage/mineCourse_page.dart';
import 'dart:ui';

// void main() => runApp(MyHomePage());
void main ( ) => runApp(_widgetForRoute(window.defaultRouteName));

Widget _widgetForRoute(String route) {

  switch (route) {

    case 'rouete1':
      return MaterialApp(home: Index());

    case 'rouete2':
      return MaterialApp(home: MineCoursePage());

    default:
      return Center(
        child: Text('Unknown route: $route ，Please pass in a valid route name', textDirection: TextDirection.ltr),
      );
  }
}



class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // home: MineCoursePage(),
       home: Index(),
      // home: ListViewPage(),
      // home: TabbarGoodCourse(),
    );
  }
}