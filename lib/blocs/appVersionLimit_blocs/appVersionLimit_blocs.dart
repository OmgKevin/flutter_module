
import 'dart:async';
import 'package:flutter_module/provider/base/base_resp.dart';
import 'package:flutter_module/provider/resource/appversionlimit_api_provider.dart';
import 'package:flutter_module/utils/singleton_util.dart';

TransferDataSingleton singlemanager = new TransferDataSingleton();

class AppVersionLimitBloc {

  final provider = AppVersionLimitApiProvider();
  StreamSubscription<BaseResp> _appVersionLimitSubcrition;
  final StreamController<BaseResp> _appVersionLimitController = StreamController<BaseResp>.broadcast();
  Stream<BaseResp> get appVersionLimit => _appVersionLimitController.stream;

 
  void getAppVersionLimit (params) {
    _appVersionLimitSubcrition?.cancel();
    _appVersionLimitSubcrition = provider.getAppVersionLimit(params).asStream().listen((BaseResp data) {

      if (data.result) {

        // eventBus.fire(new EventBusMessage(data.data['limitType']));

        singlemanager.appVersion = data.data['limitType'];
      }
      _appVersionLimitController.add(data);
    });
  }

  void dispose() {
    _appVersionLimitSubcrition?.cancel();
    _appVersionLimitController.close();
  }
}