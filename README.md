# flutter_module

A new flutter module project.

 

## 使用说明：

- [首页URL数据接口] http://172.18.70.102:8200/sunlands-app-api/


- [MethodChannel方法通道名] :  

首页通道名 :  "com.sunlands.your/flutter_homepage"  
我的课程通道名 : "com.sunlands.your/flutter_mycourses"

## 首页：
- 首页flutter页面通过方法通道跳转下级页面方法名

1.首页广告位banner : adbanner

2.首页试听课banner : triallessonbanner

3.首页试听课列表 : triallessonlist

4.首页IEA认证项目banner : iealessonbanner

5.首页IEA认证项目列表 : iealessonlist



## 我的课程：
- 我的课程flutter页面通过方法通道跳转下级页面方法名

1.我的课程列表点击跳转next页面 : mycourseslist



## 传参说明：

- [NativeToFlutter] 

1.路由名必传 :  
  1.1 首页对应路由名  “rouete1”   
  1.2 我的课程对应路由名   “rouete2”



iOS示例： {
        @"page":@"“rouete2”",                        
        
        };


- [FlutterToNative] 





## 项目结构

-- pubspec.yaml <!-- 该文件集中管理了本项目中所依赖的第三方库，以及图片，文字资源等-->
-- assets <!--本地资源集中管理-->
  -- images <!--本地图片资源集中管理-->
    -- homepage <!--首页使用的本地图片资源-->
-- lib <!-- 这里是 Flutter 的代码，使用 Dart 语言编写。-->
  -- main.dar <!-- flutter项目的主程序入口-->
  -- screens <!--flutter项目的view层代码，以页面为层级区分-->
    -- indexPage <!--首页内部UI组件-->
      -- home_screen.dart <!--首页上部切换tab组件-->
      -- index_openCourse.dart <!--首页中下部banner和列表组件-->
  -- blocs <!--数据响应式流处理，状态管理, 以页面为层级区分-->
    -- homepage_blocs <!--首页的数据管理-->
      -- openclasstype_bloc.dart <!--首页课程类型数据管理-->
      -- openclass_bloc.dart <!--首页课程列表数据管理-->
  -- provider <!--数据请求相关-->
    -- base <!--请求方法封装、api相关常量和变量的集中管理-->
      -- base_api_provider.dart <!--基于flutter自带的Dio进行post和get请求的封装，也包括请求拦截等-->
      -- base_resp.dart <!--对接口返回数据进行转化，相当于model-->
      -- api_config.dat <!--api相关常量的管理-->
      -- api_config.dart <!--各个页面的api接口地址的同意管理-->
    -- resource <!--各个页面的请求行为-->
      -- homepage_api_provider.dart <!--首页各个接口的请求行为，Future封装相当于前端的Promise-->
  -- models <!--接口请求数据的转换，以页面为层级-->
    -- indexPage_models <!--首页接口请求数据的转换-->
    -- mineCoursePage_models <!--我的课程接口请求数据的转换-->     
  -- utils <!--各种公用工具类的集中管理-->
    -- screen_util.dart <!--屏幕信息相关工具类-->
    -- sign_util.dart <!--接口提交数据签名相关的工具类-->
    -- date_util.dart <!--时间相关工具类-->
  -- widgets <!--各种公共组件的集中管理-->
    -- loading.dart <!--加载中标识组件-->
    -- error.dart <!--接口报错提示组件-->


   ## 封装类用法：

   1. shared_util.dart

  Tips: 基于shared_preferences进行的二次封装，可保存String,int,Bool,Double,StringList等结构类型
  
  用法：
  存： await SharedUtil.instance.saveString(Keys.authorization, user.authorization);
  
  取： String authorization =await _getSharedPerfencesString(Keys.authorization);
  
    _getSharedPerfencesString(String key) async {
    if (_sharedPreferences == null) {
      _sharedPreferences = await SharedPreferences.getInstance();
    }
    return _sharedPreferences.get(key) ?? "";
  }
